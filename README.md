This is a library project for Qt android serial port. It consists of the following parts:

- The cpp source code which is a part of QtSerialPort module of the Qt Toolkit.
- Java modules which is mostly from Google  (Copyright 2013 Google Inc.). Some modifications had also been made by other authors (you can see detail from the comments in those Java files).

Note: Some additional minor modifications were made to this libraries


By default, Java files won't be compiled. You might need to use javac to do it or just comment out the statement "TEMPLATE = lib" in qtandroidserialport.pro to let Qt generate intermediate class files (then use archive manager, for example, to pack all of the class files into .jar) or classes.dex (be carefull about qt embedded class files in this case) in the output location so you can use it with the cpp part
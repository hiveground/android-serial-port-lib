TARGET = AndroidSerialPortLib
TEMPLATE = lib #comment this out to generate class files from java
QT += androidextras \
      core-private

DEFINES += QTANDROIDSERIALPORT_LIBRARY

INCLUDEPATH += $$PWD
ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

PUBLIC_HEADERS += \
    $$PWD/qserialport.h \
    $$PWD/qserialportinfo.h

PRIVATE_HEADERS += \
    $$PWD/qserialport_p.h \
    $$PWD/qserialportinfo_p.h \
    $$PWD/qserialport_android_p.h

SOURCES += \
    $$PWD/qserialport.cpp \
    $$PWD/qserialportinfo.cpp \
    $$PWD/qserialport_android.cpp \
    $$PWD/qserialportinfo_android.cpp

DISTFILES += \
    $$PWD/android/src/com/hoho/android/usbserial/driver/CdcAcmSerialDriver.java \
    $$PWD/android/src/com/hoho/android/usbserial/driver/CommonUsbSerialDriver.java \
    $$PWD/android/src/com/hoho/android/usbserial/driver/Cp2102SerialDriver.java \
    $$PWD/android/src/com/hoho/android/usbserial/driver/FtdiSerialDriver.java \
    $$PWD/android/src/com/hoho/android/usbserial/driver/ProlificSerialDriver.java \
    $$PWD/android/src/com/hoho/android/usbserial/driver/UsbId.java \
    $$PWD/android/src/com/hoho/android/usbserial/driver/UsbSerialDriver.java \
    $$PWD/android/src/com/hoho/android/usbserial/driver/UsbSerialProber.java \
    $$PWD/android/src/com/hoho/android/usbserial/driver/UsbSerialRuntimeException.java \
    $$PWD/android/src/org/androidSerialPortLib/helper/UsbDeviceJNI.java \
    $$PWD/android/src/org/androidSerialPortLib/helper/UsbIoManager.java

CONFIG += mobility

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS \
    qtandroidserialport_global.h
